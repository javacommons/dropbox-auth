package jc.dropbox.auth.test;

import com.dropbox.core.v2.DbxClientV2;
import jc.dropbox.auth.DropboxAuth;

public class Main {

    public static void main(String[] args) throws Exception {
        DropboxAuth auth = new DropboxAuth();
        final DbxClientV2 client = auth.getClient();
        if (client == null) {
            System.out.println("Auth Error!");
            System.exit(1);
        }
        System.out.println("Capacity: "
                + client.users().getSpaceUsage().getAllocation().getIndividualValue().getAllocated() / 1024 / 1024 + " MB");
        System.out.println("Usage: " + client.users().getSpaceUsage().getUsed() / 1024 / 1024 + " MB");
    }

}
