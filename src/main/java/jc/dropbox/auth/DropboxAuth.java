package jc.dropbox.auth;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxSessionStore;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.oauth.DbxCredential;
import com.dropbox.core.v2.DbxClientV2;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.eclipse.jetty.server.Connector;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class DropboxAuth /*extends AbstractHandler*/ {

    String APP_NAME;
    String APP_KEY;
    String APP_SECRET;
    DbxRequestConfig CONFIG;
    DbxAppInfo APP_INFO;
    String AUTH_FILE;
    int PORT_MIN;
    int PORT_MAX;
    DropboxAuthHandler handler = null;

    protected void init(String appName, String appKey, String appSecret, String authFilePath, int portMin, int portMax) {
        this.APP_NAME = appName;
        this.APP_KEY = appKey;
        this.APP_SECRET = appSecret;
        this.CONFIG = DbxRequestConfig.newBuilder(this.APP_NAME).build();
        this.APP_INFO = new DbxAppInfo(this.APP_KEY, this.APP_SECRET);
        this.AUTH_FILE = authFilePath;
        this.PORT_MIN = portMin;
        this.PORT_MAX = portMax;
    }

    public DropboxAuth() throws Exception {
        PropertiesConfiguration config = new PropertiesConfiguration(System.getenv("HOME") + "/dropbox-auth.properties");
        this.init(config.getString("APP_NAME"), config.getString("APP_KEY"),
                config.getString("APP_SECRET"), config.getString("AUTH_FILE"),
                config.getInt("PORT_MIN"), config.getInt("PORT_MAX"));
    }

    public DropboxAuth(String appName, String appKey, String appSecret, String authFilePath, int portMin, int portMax) {
        this.init(appName, appKey, appSecret, authFilePath, portMin, portMax);
    }

    private DbxCredential checkAuthFile() {
        try {
            final DbxCredential credential = DbxCredential.Reader.readFromFile(this.AUTH_FILE);
            DbxClientV2 client = new DbxClientV2(this.CONFIG, credential.getAccessToken());
            final long allocated = client.users().getSpaceUsage().getAllocation().getIndividualValue().getAllocated();
            //System.out.println("Capacity: "
            //        + client.users().getSpaceUsage().getAllocation().getIndividualValue().getAllocated() / 1024 / 1024 + " MB");
            final long used = client.users().getSpaceUsage().getUsed();
            //System.out.println("Usage: " + client.users().getSpaceUsage().getUsed() / 1024 / 1024 + " MB");
            return credential;
        } catch (Exception ex) {
            return null;
        }
    }

    public DbxClientV2 getClient() {
        DbxCredential credential = this.authorize();
        DbxClientV2 client = new DbxClientV2(this.CONFIG, credential.getAccessToken());
        return client;
    }

    public DbxCredential authorize() {
        DbxCredential credential = checkAuthFile();
        if (credential != null) {
            return credential;
        }
        Server server = null;
        for (int p = this.PORT_MIN; p <= this.PORT_MAX; p++) {
            //server = new Server(p);
            // https://stackoverflow.com/questions/1955455/how-to-secure-jetty-to-only-allow-access-from-loopbacklocalhost
            server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(p);
            connector.setHost("localhost");
            server.setConnectors(new Connector[]{connector});
            this.handler = new DropboxAuthHandler();
            server.setHandler(handler);
            try {
                server.start();
                break;
            } catch (Exception ex) {
                this.handler = null;
                server = null;
                continue;
            }
        }
        if (server == null) {
            System.err.println("Could not start Jetty seerver");
            return null;
        }
        try {
            final int port = server.getURI().getPort();
            System.out.println(port);
            final String redirectUri = "http://localhost:" + port;
            System.out.println(redirectUri);
            DbxSessionStore sessionStore = new SimpleSessionStore();
            DbxWebAuth auth = new DbxWebAuth(CONFIG, APP_INFO);
            String authUrl = auth.authorize(
                    DbxWebAuth.newRequestBuilder()
                            .withRedirectUri(redirectUri, sessionStore)
                            .build()
            );
            System.out.println(authUrl);
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(new URI(authUrl));
            }
            while (this.handler.params == null) {
                Thread.sleep(100);
            }
            System.out.println(this.handler.code);
            DbxAuthFinish authFinish = auth.finishFromRedirect(redirectUri, sessionStore, this.handler.params);
            //DbxClientV2 client = new DbxClientV2(CONFIG, authFinish.getAccessToken());
            credential = new DbxCredential(authFinish.getAccessToken(), authFinish
                    .getExpiresAt(), authFinish.getRefreshToken(), APP_INFO.getKey(), APP_INFO.getSecret());
            File output = new File(AUTH_FILE);
            try {
                DbxCredential.Writer.writeToFile(credential, output);
                System.out.println("Saved authorization information to \"" + output.getCanonicalPath() + "\".");
                String s = DbxCredential.Writer.writeToString(credential);
                System.err.println(s);
            } catch (IOException ex) {
                System.err.println("Error saving to <auth-file-out>: " + ex.getMessage());
                System.err.println("Dumping to stderr instead:");
                DbxCredential.Writer.writeToStream(credential, System.err);
                System.exit(1);
                return null;
            }
            return credential;
        } catch (Exception ex) {
            Logger.getLogger(DropboxAuth.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            try {
                server.stop();
            } catch (Exception ex) {
                ;
            }
        }
    }

    private static final class DropboxAuthHandler extends AbstractHandler {

        public Map<String, String[]> params = null;
        public String code = null;

        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            System.out.println("target = " + target);
            if (!target.equals("/")) {
                response.setContentType("text/html;charset=utf-8");
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                baseRequest.setHandled(true);
                response.getWriter().println("<h1>NOT FOUND!</h1>");
                return;
            }
            System.out.println(request.getParameterMap());
            this.params = request.getParameterMap();
            this.code = request.getParameter("code");
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
            response.getWriter().println("<html><head><title>DropBox Auth</title></head><body><h1>DropBox Auth Successful!</h1><p>You can close this window now.</p></body>");
        }
    }

    private static final class SimpleSessionStore implements DbxSessionStore {

        private String token;

        public SimpleSessionStore() {
            this.token = null;
        }

        @Override
        public String get() {
            return token;
        }

        @Override
        public void set(String value) {
            this.token = value;
        }

        @Override
        public void clear() {
            this.token = null;
        }
    }

}
